﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour
{
    public Vector3 pick;

    public GameObject cardPrefab;
    

    private Camera _mainCamera;
    private int _cardInPick, _clicked;



    public void Start()
    {
        pick = new Vector3(1.45f, 0, 0);
        _mainCamera = Camera.main;

    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("J'ai cliqué");
            
            Vector3 mousePos = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null)
            {
                _clicked++;
                Debug.Log(_clicked);
                hit.collider.transform.position = pick;


            }
        }
    }
}
