﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace ChangePlayer
{
    public class GameManager : StateMachine
    {
        public GameObject pioche, gameManager;

        void Awake() 
        {
            SetState(new Begin(this)); //State qui se joue qu'une fois au début de la partie
        }

        public void OnPlayCard() //A chaque fois qu'on clique sur une carte
        {
            StartCoroutine(State.Play()); //lance la state qui permet le changement de joueur
        }
    }
}