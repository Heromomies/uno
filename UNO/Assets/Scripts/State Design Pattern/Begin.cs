﻿using System.Collections;

namespace ChangePlayer
{
    public class Begin : State
    {
        public Begin(GameManager gameManager) : base(gameManager)
        {
        }

        public override IEnumerator Start()
        {
            GameManager.pioche.GetComponent<Pioche>().DistributionPlayer1(); //Lance la fonction qui permet de distribué 7 cartes au Joueur 1
            GameManager.pioche.GetComponent<Pioche>().DistributionPlayer2();//Lance la fonction qui permet de distribué 7 cartes au Joueur

            GameManager.SetState(new TurnPlayer1(GameManager)); //lance la sate qui permet au joueur 1 de jouer
            yield break;
        }
    }
}