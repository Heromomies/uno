﻿using System.Collections;
using UnityEngine;

namespace ChangePlayer
{
    public class TurnPlayer2 : State
    {
        public TurnPlayer2(GameManager gameManager) : base(gameManager) 
        {
        }

        public override IEnumerator Start() //se joue quand on rentre la state
        {
            GameManager.pioche.GetComponent<Pioche>().RetournéeCarte(2); //permet d'afficher les cartes du joueur et retourner celles de l'adversaire
            GameManager.gameManager.GetComponent<OnClickCards>().HandPickPlayer(2);
            yield break;
        }
        
        public override IEnumerator Play()
        {
            GameManager.SetState(new TurnPlayer1(GameManager)); //passe à l'autre state donc changement de joueur
            yield break;
        }
    }
}