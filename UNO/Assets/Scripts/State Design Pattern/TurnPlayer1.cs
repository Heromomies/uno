﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace ChangePlayer
{
    public class TurnPlayer1 : State
    {
        public TurnPlayer1(GameManager gameManager) : base(gameManager)
        {
        }

        public override IEnumerator Start() //se joue quand on rentre la state
        {
            GameManager.pioche.GetComponent<Pioche>().RetournéeCarte(1); //permet d'afficher les cartes du joueur et retourner celles de l'adversaire
            GameManager.gameManager.GetComponent<OnClickCards>().HandPickPlayer(1);
            yield break;
        }

        public override IEnumerator Play()
        {
            GameManager.SetState(new TurnPlayer2(GameManager)); //passe à l'autre state donc changement de joueur
            yield break;
        }
    }
}