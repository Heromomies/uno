﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ChangePlayer
{
    public abstract class State
    {
        protected GameManager GameManager;
        
        //toute les states utilisées
        public State(GameManager gameManager)
        {
            GameManager = gameManager;
        }

        public virtual IEnumerator Start()
        {
            yield break;
        }
        
        public virtual IEnumerator Play()
        {
            yield break;
        }
        
        public virtual IEnumerator Win()
        {
            yield break;
        }
    }   
}

