﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCard : MonoBehaviour
{
    public Sprite carteRetournée;
    
    public Cards cards;

    private string _color;
    private int _number;
    public int id;
    private Bonus _bonus;

    private Sprite _sprite;
    public SpriteRenderer _spriteRenderer;

    void Awake()
    { 
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        _color = cards.colorCards;
        _number = cards.numberCards;
        _bonus = cards.bonus;
        _sprite = cards.spriteCards;
    }

    public void CarteRetournée() //Quand on veut afficher le dos d'une cate
    {
        _spriteRenderer.sprite = carteRetournée; //met un sprite prédéfinis
    }

    public void CarteNonRetournée() //Quand on veut affichre le contenu de la carte
    {
        _sprite = cards.spriteCards;
        _spriteRenderer.sprite = _sprite; //met le sprite grâce aux valeur du scriptableobject
    }
}
