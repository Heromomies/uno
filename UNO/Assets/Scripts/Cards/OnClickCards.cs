﻿using System;
using System.Collections;
using System.Collections.Generic;
using ChangePlayer;
using UnityEngine;

public class OnClickCards : MonoBehaviour
{
    public Vector3 pick;
    public List<Cards> cardsInPick = new List<Cards>();
    public List<Cards> cardsInDeck = new List<Cards>();
    public List<Cards> handP1OCC = new List<Cards>(); //carte que possède le joueur 1 dans sa main
    public List<Cards> handP2OCC = new List<Cards>();
    public GameObject cardPrefab;
    public Pioche pioche;
    public Transform panelTas, panelHandP1OCC, panelHandP2OCC;
    
    private Cards _cards;
    private Camera _mainCamera;
    private SetCard _setCard;
    private int _cardInPick, _numberCards;

    public GameManager gameManager;

    public void Start()
    {
        pick = new Vector3(1.45f, 0, 0);
        _mainCamera = Camera.main;
        gameManager = FindObjectOfType<GameManager>();
        pioche = FindObjectOfType<Pioche>();
        cardsInDeck = pioche.GetComponent<Pioche>().deck;
        handP1OCC = pioche.GetComponent<Pioche>().handP1;
        handP2OCC = pioche.GetComponent<Pioche>().handP2;
        panelHandP1OCC = pioche.GetComponent<Pioche>().panelHandP1;
        panelHandP2OCC = pioche.GetComponent<Pioche>().panelHandP2;
        _numberCards = _setCard.GetComponent<SetCard>().cards.numberCards;
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null && hit.collider.GetComponent<SetCard>().id == 2 || hit.collider != null && hit.collider.GetComponent<SetCard>().id == 1 ||  hit.collider != null && hit.collider.GetComponent<SetCard>().id == 3)
            {
                hit.collider.transform.position = pick;
                Debug.Log("Id = 1 or 2");
                switch (hit.collider.GetComponent<SetCard>().cards.numberCards)
                {
                    case 0 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 1 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 2 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 3: Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 4 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 5 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 6 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 7 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 8 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 9 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    case 10 : Debug.Log(hit.collider.GetComponent<SetCard>().cards.numberCards);
                        break;
                    default: Debug.Log("error number");
                        break;
                }
                switch (hit.collider.GetComponent<SetCard>().cards.colorCards)
                {
                    case "blue": Debug.Log(hit.collider.GetComponent<SetCard>().cards.colorCards);
                        break;
                    case "red": Debug.Log(hit.collider.GetComponent<SetCard>().cards.colorCards);
                        break;
                    case "yellow": Debug.Log(hit.collider.GetComponent<SetCard>().cards.colorCards);
                        break;
                    case "green": Debug.Log(hit.collider.GetComponent<SetCard>().cards.colorCards);
                        break;
                    case "white": Debug.Log(hit.collider.GetComponent<SetCard>().cards.colorCards);
                        break;
                    default: Debug.Log("error color");
                        break;
                }
                cardsInPick.Add(handP2OCC[hit.collider.shapeCount]);
                handP2OCC.RemoveAt(hit.collider.shapeCount);
                GameObject card = Instantiate(cardPrefab);
                card.transform.SetParent(panelTas.transform,false);
                card.GetComponent<SetCard>().id = 4;
                card.GetComponent<SetCard>().cards = cardsInPick[hit.collider.shapeCount];
                
                gameManager.GetComponent<GameManager>().OnPlayCard(); //appel la fonction qui va permettre le changement de joueur
            }
            if (hit.collider != null && hit.collider.GetComponent<SetCard>().id == 0)
            {
                Debug.Log("Id = 0");
                HandPickPlayer(2);
                gameManager.GetComponent<GameManager>().OnPlayCard(); //appel la fonction qui va permettre le changement de joueur
            }
        }
    }

    public void HandPickPlayer(int player)
    {
        if (player == 1)
        {
            Debug.Log("Player 1 ajout carte");
            
            handP1OCC.Add(cardsInDeck[1]);
            cardsInDeck.RemoveAt(1);
            GameObject card = Instantiate(cardPrefab);
            card.transform.SetParent(panelHandP1OCC.transform,false);
            card.GetComponent<SetCard>().id = 1;
            card.GetComponent<SetCard>().cards = handP1OCC[1];
        }
        else if (player == 2)
        {
            Debug.Log("Player 2 ajout carte");

            handP2OCC.Add(cardsInDeck[1]);
            cardsInDeck.RemoveAt(1);
            GameObject card = Instantiate(cardPrefab);
            card.transform.SetParent(panelHandP2OCC.transform,false);
            card.GetComponent<SetCard>().id = 2;
            card.GetComponent<SetCard>().cards = handP2OCC[1];
        }
    }
}
