﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum Bonus // type d'action 
{
    SensChangement,
    ChangeColor,
    Add2Cards,
    Add4Cards,
    Interdiction,
    Color
}
[CreateAssetMenu(fileName = "Normal Cards")]
public class Cards : ScriptableObject
{
    public Sprite spriteCards;
    public int numberCards;
    public string colorCards;
    public Bonus bonus;
    public int idCards;
}