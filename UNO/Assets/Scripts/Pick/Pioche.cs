﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pioche : MonoBehaviour
{
    public Deck myDeck;

    public int PremiereDistribution = 7;
    public int pickDistribution = 47;
    
    public List<Cards> handP1 = new List<Cards>(); //carte que possède le joueur 1 dans sa main
    public List<Cards> handP2 = new List<Cards>(); //carte que possède le joueur 2 dans sa main
    public List<Cards> deck = new List<Cards>(); //tout les cartes disponible
    public List<Cards> cardInPick = new List<Cards>();
    
    public GameObject cardPrefab;
    public Transform panelHandP1, panelHandP2, panelPick;
    public List<GameObject> cardP1, cardP2;

    void Awake()
    {
        Deck.Shuffle(myDeck);

        for (int i = 0; i < pickDistribution; i++)
        {
            deck.Add(myDeck.cards[i]);
            myDeck.cards.RemoveAt(i);
            GameObject card = (GameObject) Instantiate(cardPrefab);
            card.transform.SetParent(panelPick.transform,false);
            card.GetComponent<SetCard>().id = 3;
            card.GetComponent<SetCard>().cards = deck[i];
        }    
    }

    public void DistributionPlayer1()
    {
        for (int i = 0; i < PremiereDistribution; i++) //tant qu'il n'y a pas 7 cartes
        {
            handP1.Add(myDeck.cards[i]); //ajoute une carte dans la liste "hand"
            myDeck.cards.RemoveAt(i); //enlève la carte en qestion dans le deck
            GameObject card = (GameObject) Instantiate(cardPrefab); //fait apparaitre le perfabs de la carte en question
            cardP1.Add(card);
            card.transform.SetParent(panelHandP1.transform, false); //ajoute la carte dans le panel
            card.GetComponent<SetCard>().id = 1;
            card.GetComponent<SetCard>().cards = handP1[i]; //pour lui afficher son sprite
        }
    }

    public void DistributionPlayer2()
    {
        for (int i = 0; i < PremiereDistribution; i++) 
        {
            handP2.Add(myDeck.cards[i]);
            myDeck.cards.RemoveAt(i);
            GameObject card = (GameObject) Instantiate(cardPrefab);
            cardP2.Add(card);
            card.transform.SetParent(panelHandP2.transform, false);
            card.GetComponent<SetCard>().id = 2;
            card.GetComponent<SetCard>().cards = handP2[i];
        }
    }

    public void RetournéeCarte(int player)
    {
        if (player == 1)
        {
            Debug.Log("PlayerValue : " + player);
            foreach (var card in cardP1)
            {
                card.GetComponent<SetCard>().CarteNonRetournée();
            }

            foreach (var card in cardP2)
            {
                card.GetComponent<SetCard>().CarteRetournée();
            }
        }
        else
        {
            foreach (var card in cardP2)
            {
                card.GetComponent<SetCard>().CarteNonRetournée();
            }

            foreach (var card in cardP1)
            {
                card.GetComponent<SetCard>().CarteRetournée();
            }
        }
    }
}