﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ System.Serializable ]
public class Deck : MonoBehaviour
{
    public List< Cards > cards; // List of cards
    public Deck() { cards = new List<Cards>(); } // Default constructor to initialize the list.
    
    // Method to shuffle a Deck using Fisher-Yates shuffle.
    public static void Shuffle( Deck deck )
    {
        System.Random random = new System.Random();
 
        for( int i = 0; i < deck.cards.Count; i ++ ) {
            int j = random.Next( i, deck.cards.Count );
            Cards temporary = deck.cards[ i ];
            deck.cards[ i ] = deck.cards[ j ];
            deck.cards[j] = temporary;
        }
        /*foreach(Cards cards in deck.cards) {
            Debug.Log( cards.ToString());
            //cards.ToString();
        }*/
    }
}

