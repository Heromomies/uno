﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI1;

    void Update() // Appuyer sur la touche Echap pour : 
    {
        if (Input.GetKeyDown(KeyCode.Escape))

            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }

    }

    public void Resume() // Remet le jeu 
    {
        pauseMenuUI1.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause() // Freeze le temps et le jeu
    {
        pauseMenuUI1.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Restart() // Retourne au début
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1f;
        PlayerPrefs.SetInt("Score", 0);
    }

    public void LoadMenu() // retourne au Menu
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void QuitGame() // Quitte le jeu
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }
}
